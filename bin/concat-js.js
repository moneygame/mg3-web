var concatTag = '<!-- concat:js -->';
var appName = 'moneygame';
var destDirectory = 'www/';

function concatJS() {
  var fs = require('fs-extra');
  var UglifyJs = require('uglify-js');

  var indexStr = fs.readFileSync('index.html', 'utf-8');
  var scriptsStr = indexStr.match(new RegExp(concatTag + '[\\s\\S]*' + concatTag, 'g'));

  var scriptPathRegExp = new RegExp('<script.*src="(.*)"', 'g');
  var scriptPaths = getAllMatches(scriptPathRegExp, scriptsStr);
  var orderedScriptPaths = orderScriptPaths(scriptPaths);
  var result = UglifyJs.minify(orderedScriptPaths);

  var outputFilename = destDirectory + 'js/' + appName + '.js';
  fs.outputFileSync(outputFilename, result.code);

  var newIndexStr = indexStr.replace(scriptsStr,
      '<!-- ' + appName + ' -->\n    <script src="js/' + appName + '.js"></script>');
  fs.outputFileSync(destDirectory + 'index.html', newIndexStr);

  function getAllMatches(regexp, str) {
    var allMatches = [];
    var match = regexp.exec(str);

    while (match != null) {
      allMatches.push(match[1]);
      match = regexp.exec(str);
    }

    return allMatches;
  }

  function orderScriptPaths(scriptPaths) {
    var orderedPaths = [];
    var fileTypeRegExps = [
      /node_modules.*js/g,
      /[^\(app\)]\.module\.js/g,
      /app\.module\.js/g,
      /[^\(controller|service\)]\.js/g,
      /\.service\.js/g,
      /\.controller\.js/g
    ];

    var path;
    for (regexp of fileTypeRegExps) {
      for (var i = 0; i < scriptPaths.length; i++) {
        path = scriptPaths[i];

        if (path.match(regexp)) {
          orderedPaths.push(path);
          scriptPaths.splice(i, 1);
          i--;
        }
      }
    }

    return orderedPaths;
  }
}

module.exports = concatJS;

