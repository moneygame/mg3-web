'use strict';
var config = {
  distPath: 'www',
  filesToCopyToDist: ['audio', 'css', 'fonts', 'img', 'lib', 'index.html'],
  extensionsToCopyToDist: ['.html'],
  appPath: 'app',
  sass: {
    inputFile: 'app/app.scss',
    outputFile: 'css/style.css',
    includePaths: ['app', 'fonts']
  }
};

var fs = require('fs-extra');
var sass = require('node-sass');
var path = require('path');
var createConfig = require('./create-config.js');
var uglifyJS = require('./concat-js.js');

buildDist(config);

function buildDist(config) {
  removeExistingDist(config.distPath);

  preprocessCSS(config.sass.inputFile, config.sass.outputFile,
                                      config.sass.includePaths);

  setupEnvConfig();

  copyFilesToDist(config.filesToCopyToDist, config.extensionsToCopyToDist,
                                           config.appPath, config.distPath);

  console.log('>>> Uglifying js files');
  uglifyJS();
}

function removeExistingDist(distPath) {
  console.log('>>> Removing existing build');
  fs.removeSync(distPath);
}

function preprocessCSS(inputFile, outputFile, includePaths) {
  console.log('>>> Compiling Sass');
  var compiledSass = sass.renderSync({
      data: fs.readFileSync(inputFile).toString(),
      outFile: outputFile,
      outputStyle: 'compressed',
      includePaths: includePaths
  });
  fs.outputFileSync(outputFile, compiledSass.css);
}

/*
 * Copies all app files to the dist directory.
 */
function copyFilesToDist(filesToCopyToDist, extensionsToCopyToDist,
                                                 appPath, distPath) {
  console.log('>>> Copying necessary files to ' + distPath + '/');
  fs.ensureDirSync(distPath);
  copyAppFiles(extensionsToCopyToDist, appPath, distPath);
  copyFilesBatch(filesToCopyToDist, distPath);
}

/*
 * Creates the app.config.js file that lets angular
 * access the environment variables.
 */
function setupEnvConfig() {
  console.log('>>> Creating config');
  createConfig();
}

/*
 * Copies all files from the app code with certain extensions to the dist.
 */
function copyAppFiles(extensionsToCopyToDist, appPath, distPath) {
  for (var i = 0; i < extensionsToCopyToDist.length; i++) {
    copyByExtension(extensionsToCopyToDist[i], appPath, distPath);
  }
}

/*
 * Recursively searches for files with a certain extension in all directories
 * under "searchDirectory" and copies then to the dist path.
 */
function copyByExtension(ext, searchDirectory, destDirectory) {
  var files = fs.readdirSync(searchDirectory);
  var filename, stat;

  for (var i = 0; i < files.length; i++) {
    filename = path.join(searchDirectory, files[i]);
    stat = fs.lstatSync(filename);

    if (stat.isDirectory()) {
      copyByExtension(ext, filename, destDirectory);
    } else if (filename.indexOf(ext) >= 0) {
      fs.copySync(filename, path.join(destDirectory, filename));
    }
  }
}

/*
 * Copies files or directories to a given directory.
 */
function copyFilesBatch(filesList, destDirectory) {
  for (var i = 0; i < filesList.length; i++) {
    fs.copySync(filesList[i], path.join(destDirectory, filesList[i]));
  }
}

