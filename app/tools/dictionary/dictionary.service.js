(function() {
  'use strict';

  angular
    .module('moneygame.dictionary')
    .service('dictionaryService', dictionaryService);

  function dictionaryService($http, API_URL) {
    this.getDictionaryTerms = getDictionaryTerms;

    function getDictionaryTerms() {
      return $http.get(API_URL + 'dictionary_terms')
        .then(function(dictionaryTerms) {
          return dictionaryTerms.data.dictionary_terms;
        });
    }
  }
  dictionaryService.$inject = ['$http', 'API_URL'];
})();

