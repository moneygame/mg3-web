(function() {
  'use strict';

  angular
    .module('moneygame.dictionary')
    .controller('DictionaryController', DictionaryController);

  function DictionaryController($uibModal, $scope, $q, $state, $stateParams,
                                dictionaryService, analyticsService) {
    var vm = this;
    vm.showDefinition = showDefinition;
    vm.groupedTerms;
    vm.definitionModal;
    vm.dictionaryTerm;
    var initialIndexes = [];

    activate();

    function activate() {
      vm.fromGame = $stateParams.fromGame;
      analyticsService.setViewAnalytics($state.current.name);
      dictionaryService.getDictionaryTerms()
        .then(groupTermsByInitial);
    }

    function showDefinition(dictionaryTerm) {
      vm.dictionaryTerm = dictionaryTerm;
      analyticsService.setViewAnalytics('definition');
      vm.definitionModal = showModal('app/tools/dictionary/definition.html');
    }

    function groupTermsByInitial(dictionaryTerms) {
      vm.groupedTerms = [];
      var initial, index;

      angular.forEach(dictionaryTerms, function(dictionaryTerm) {
        initial = dictionaryTerm.term[0];
        index = initialIndexes.indexOf(initial);

        if (index === -1) {
          initialIndexes.push(initial);
          vm.groupedTerms.push({
            initial: initial,
            terms: [dictionaryTerm]
          });
        } else {
          vm.groupedTerms[index].terms.push(dictionaryTerm);
        }

      });
    }

    function showModal(templateUrl) {
      var modal = $uibModal.open({
        templateUrl: templateUrl,
        scope: $scope
      });

      return modal;
    }
  }
  DictionaryController.$inject = ['$uibModal', '$scope', '$q', '$state',
      '$stateParams', 'dictionaryService', 'analyticsService'];
})();

