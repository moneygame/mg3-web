(function() {
  'use strict';

  angular
    .module('moneygame.tools')
    .controller('ToolsController', ToolsController);

  function ToolsController($scope, $uibModal, $q, $state, loanCalculatorService,
                           savingsCalculatorService, $timeout, analyticsService, $stateParams) {
    var vm = this;
    vm.showLoanCalculator = showLoanCalculator;
    vm.showLoanCalcResult = showLoanCalcResult;
    vm.showSavingsCalculator = showSavingsCalculator;
    vm.showSavingsCalcResult = showSavingsCalcResult;
    vm.loanCalcData = {};
    vm.savingsCalcData = {};
    vm.toolModal;
    vm.resultsModal;

    activate();

    function activate() {
      vm.fromGame = $stateParams.fromGame;
      analyticsService.setViewAnalytics($state.current.name);

      $scope.$on('modal.hidden', function() {
        vm.toolModal.isShown() ? vm.resultsModal.remove() : vm.toolModal.remove();
      });
    }

    function showLoanCalculator() {
      analyticsService.setViewAnalytics('loan-calculator');
      vm.toolModal = showModal('app/tools/loan-calculator/loan-calculator.html');
    }

    function showLoanCalcResult() {
      vm.loanCalcData.payment = loanCalculatorService.calculatePayment(vm.loanCalcData);
      vm.resultsModal = showModal('app/tools/loan-calculator/results.html');
    }

    function showSavingsCalculator() {
      analyticsService.setViewAnalytics('savings-calculator');
      vm.toolModal = showModal('app/tools/savings-calculator/savings-calculator.html');
    }

    function showSavingsCalcResult() {
      vm.savingsCalcData.savingsPerYear = savingsCalculatorService.calculateSavings(vm.savingsCalcData);
      vm.resultsModal = showModal('app/tools/savings-calculator/results.html');
    }

    function showModal(templateUrl) {
      var modal = $uibModal.open({
        templateUrl: templateUrl,
        scope: $scope
      });

      return modal;
    }
  }
  ToolsController.$inject = ['$scope', '$uibModal', '$q', '$state', '$timeout',
      'loanCalculatorService', 'savingsCalculatorService', 'analyticsService', '$stateParams'];
})();

