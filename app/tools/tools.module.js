(function() {
  'use strict';

  angular
    .module('moneygame.tools', [])
    .config(routesConfiguration);

  function routesConfiguration($stateProvider) {
    $stateProvider
      .state('tools', {
        url: '/tools',
        cache: false,
        templateUrl: 'app/tools/tools.html',
        params: { 'fromGame': null },
        controller: 'ToolsController as vm'
      })
      .state('loanCalculator', {
        url: '/tools/loan-calculator',
        templateUrl: 'app/tools/loan-calculator/loan-calculator.html',
        controller: 'LoanCalculatorController as vm'
      })
      .state('savingsCalculator', {
        url: 'tools/savings-calculator',
        templateUrl: 'app/tools/savings-calculator/savings-calculator.html',
        controller: 'SavingsCalculatorController as vm'
      })
      .state('dictionary', {
        url: '/tools/dictionary',
        templateUrl: 'app/tools/dictionary/dictionary.html',
        params: { 'fromGame': null },
        controller: 'DictionaryController as vm'
      });
  }
  routesConfiguration.$inject = ['$stateProvider'];
})();

