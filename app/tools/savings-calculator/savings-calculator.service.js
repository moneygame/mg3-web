(function() {
  'use strict';

  angular
    .module('moneygame.tools')
    .service('savingsCalculatorService', savingsCalculatorService);

  function savingsCalculatorService() {
    this.calculateSavings = calculateSavings;

    function calculateSavings(data) {
      var rate = data.annualInterestRate / 100;
      var savings = data.principal;
      var depositPerCompoundPeriod = data.monthlyDeposit * 12 / data.compoundsPerYear;
      var compoundingPeriodInterest;

      var savingsPerYear = [];

      for(var i=0; i<data.years; i++) {
        for(var j=0; j<data.compoundsPerYear; j++) {
          compoundingPeriodInterest = savings * rate / data.compoundsPerYear;
          savings += compoundingPeriodInterest;
          savings += depositPerCompoundPeriod;
        }

        savingsPerYear.push(savings.toFixed(2));
      }

      return savingsPerYear;
    }
  }
})();

