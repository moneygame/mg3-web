(function() {
  'use strict';

  angular
    .module('moneygame.tools')
    .service('loanCalculatorService', loanCalculatorService);

  function loanCalculatorService() {
    this.calculatePayment= calculatePayment;

    function calculatePayment(data) {
      var monthlyRate = data.rate / 1200;
      var payments = data.years * 12;
      var monthlyPayment;

      if (monthlyRate) {
        monthlyPayment = (monthlyRate * data.amount) / ( 1 - Math.pow(1 + monthlyRate, -payments) );
      } else {
        monthlyPayment = data.amount / (data.years * 12);
      }

      return monthlyPayment.toFixed(2);
    }
  }
})();

