(function() {
  'use strict';

  angular
    .module('moneygame.auth')
    .service('authService', authService);

  function authService($q, userService) {
    this.loginFB = loginFB;
    this.loginGuest = loginGuest;
    this.verifyLoggedIn = verifyLoggedIn;

    function loginFB(successCallback, failureCallback) {
      FB.login(
          function(response) {
            loginFBSuccess(response, successCallback, failureCallback);
          },
          { scope: ['email', 'public_profile'] }
        );
    }

    function loginGuest() {
      userService.setGuestUser();
    }

    function verifyLoggedIn(successCallback, failureCallback) {
      FB.getLoginStatus(function(response) {
        if (response.status == 'connected') {
          loginFBSuccess(response, successCallback, failureCallback);
        } else {
          failureCallback();
        }
      });
    }

    function loginFBSuccess(fbResponse, successCallback, failureCallback) {
      var authResponse;

      if (!fbResponse.authResponse) {
        failureCallback('Cannot find the authResponse');
      } else {
        authResponse = fbResponse.authResponse;

        getUserFBInfo(authResponse)
          .then(function() {
            successCallback(fbResponse);
          });
      }
    }

    function getUserFBInfo(authResponse) {
      var info = $q.defer();

      FB.api(
        '/me',
        {
          fields: [
            'email',
            'first_name',
            'picture.width(400).height(400)',
            'age_range',
            'gender',
            'location',
            'birthday',
            'devices'
          ],
          access_token: authResponse.accessToken
        },
        function(userInfo) {
          info.resolve(userInfo);
        }
      );

      return info.promise.then(function(userInfo) {
        userService.setFBUser(userInfo);
      });
    }
  }
  authService.$inject = ['$q', 'userService'];
})();

