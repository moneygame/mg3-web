(function() {
  'use strict';

  angular
    .module('moneygame.auth', [])
    .config(routesConfiguration);

  function routesConfiguration($stateProvider) {
    $stateProvider
      .state('login', {
        url: '/',
        templateUrl: 'app/auth/auth.html',
        controller: 'AuthController as vm',
        public: true
      })
      .state('login-pml', {
        url: '/pml',
        templateUrl: 'app/auth/auth-pml.html',
        controller: 'AuthController as vm',
        public: true
      });
  }
  routesConfiguration.$inject = ['$stateProvider'];
})();

