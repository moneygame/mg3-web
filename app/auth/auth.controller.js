(function() {
  'use strict';

  angular
    .module('moneygame.auth')
    .controller('AuthController', AuthController);

  function AuthController($state, $q, authService) {
    var vm = this;
    vm.continueWithoutFB = continueWithoutFB;
    vm.loginFB = loginFB;

    function continueWithoutFB() {
      authService.loginGuest();
      $state.go('home');
    }

    function loginFB() {
      authService.loginFB(loginFBSuccess, loginFBFailure);
    }

    function loginFBSuccess() {
      $state.go('home');
    }

    function loginFBFailure() {
      alert('Error logging in. Try again.');
    }
  }
  AuthController.$inject = ['$state', '$q', 'authService'];
})();

