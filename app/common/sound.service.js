(function() {
  'use strict';

  angular
    .module('moneygame')
    .service('soundService', soundService);

  function soundService() {
    this.playNegativeSound = playNegativeSound;
    this.playPositiveSound = playPositiveSound;
    this.preloadSounds = preloadSounds;

    var numPositiveSounds;
    var numNegativeSounds;
    var negativeSounds = [];
    var positiveSounds = [];

    function playNegativeSound() {
      var random = Math.floor(Math.random() * numNegativeSounds);
      negativeSounds[random].play();
    }

    function playPositiveSound() {
      var random = Math.floor(Math.random() * numPositiveSounds);
      positiveSounds[random].play();
    }

    function preloadSounds() {
      positiveSounds.push(new Audio('audio/gtz-pos01.mp3'));
      positiveSounds.push(new Audio('audio/gtz-pos02.mp3'));
      positiveSounds.push(new Audio('audio/gtz-pos03.mp3'));
      positiveSounds.push(new Audio('audio/gtz-pos04.mp3'));
      positiveSounds.push(new Audio('audio/gtz-pos05.mp3'));
      numPositiveSounds = 5;

      negativeSounds.push(new Audio('audio/gtz-neg01.mp3'));
      negativeSounds.push(new Audio('audio/gtz-neg02.mp3'));
      negativeSounds.push(new Audio('audio/gtz-neg03.mp3'));
      negativeSounds.push(new Audio('audio/gtz-neg04.mp3'));
      negativeSounds.push(new Audio('audio/gtz-neg05.mp3'));
      numNegativeSounds = 5;
    }
  }
})();

