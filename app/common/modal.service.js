(function() {
  'use strict';

  angular
    .module('moneygame')
    .service('modalService', modalService);

  function modalService($uibModal) {
    this.showModal = showModal;

    function showModal(scope, backdropOption, templateUrl, modalSize) {
      var modal = $uibModal.open({
        templateUrl: templateUrl,
        scope: scope,
        size: modalSize,
        backdrop: backdropOption
      });

      return modal;
    }
  }
  modalService.$inject = ['$uibModal'];
})();

