(function() {
  'use strict';

  angular
    .module('moneygame')
    .service('userService', userService);

  function userService($http, $q, API_URL) {
    this.getUser = getUser;
    this.logout = logout;
    this.getLocalPlayer = getLocalPlayer;
    this.setFBUser = setFBUser;
    this.setGuestUser = setGuestUser;
    this.updateUserGameProgress = updateUserGameProgress;
    this.levelUpPlayerModuleLevel = levelUpPlayerModuleLevel;

    var localUser = {};
    var deferredUserData = $q.defer();
    var userDataPromise = deferredUserData.promise;

    activate();

    function activate() {
      initUser();
    }

    function getUser() {
      return userDataPromise.then(function() {
        return localUser;
      });
    }

    function logout() {
      initUser();
    }

    function setFBUser(userInfo) {
      var fbUserData = {
        player: {
          facebook_id: userInfo.id,
          email: userInfo.email,
          gender: userInfo.gender,
          birthdate: userInfo.birthday,
          address: userInfo.location,
          name: userInfo.first_name,
          avatar_url: userInfo.picture.data.url,
          device_name: getFirstDeviceName(userInfo.devices)
        }
      };

      return registerUser(fbUserData);
    }

    function getFirstDeviceName(devices) {
      return (devices == undefined) ? null : devices[0].os;
    }

    function setGuestUser() {
      return registerUser(localUser);
    }

    function levelUpPlayerModuleLevel(currentLevel) {
      angular.forEach(localUser.player.module_levels, function (level, key) {
        if(level.game_module_id === currentLevel.module_level.game_module_id){
          localUser.player.module_levels[key] = currentLevel.module_level;
          return;
        }
      });
    }

    function updateUserGameProgress(data) {
      localUser.player.points += data.pointsIncrease;
      localUser.player.level += data.levelIncrease;
      registerProgressUpdate();
    }

    function getLocalPlayer() {
      return localUser;
    }

    function initUser() {
      setUser({
        player: {id: '', facebook_id: ''},
        player_id: window.localStorage.getItem('mg_user') || ''
      });
    }

    function registerProgressUpdate() {
      return $http.put(API_URL + 'players/' + localUser.player.id + '/level_up', localUser);
    }

    function registerUser(localUserData) {
      return $http({
        method: 'PUT',
        url: API_URL + 'players/login',
        data: JSON.stringify(localUserData)
      })
      .then(function(response) {
        var completeUserData = response.data;
        setUser(completeUserData);
        deferredUserData.resolve();
        if (!window.localStorage.getItem('mg_user')) {
          window.localStorage.setItem('mg_user', completeUserData.player.id);
        }
      });
    }

    function setUser(user) {
      localUser = user;
    }
  }
  userService.$inject = ['$http', '$q', 'API_URL'];
})();

