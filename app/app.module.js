(function() {
  'use strict';

  angular
    .module('moneygame', [
      'ui.router',
      'ui.bootstrap',
      'moneygame.auth',
      'moneygame.home',
      'moneygame.profile',
      'moneygame.analytics',
      'moneygame.tools',
      'moneygame.game',
      'moneygame.dictionary'
    ])
    .constant('GOOGLE_ANALYTICS_ID', 'UA-81282475-2')
    .config(['$locationProvider', '$urlRouterProvider', configRouteSettings])
    .run(['$window', '$state', 'authService', 'GOOGLE_ANALYTICS_ID', 'FB_APP_ID', initApp]);

  function configRouteSettings($locationProvider, $urlRouterProvider) {
    $locationProvider.html5Mode(true);
    $urlRouterProvider.when('/', '/login').otherwise('/', '/login');
  }

  function initApp($window, $state, authService, GOOGLE_ANALYTICS_ID, FB_APP_ID) {
    setUpFB($window, authService, verifyLoginSuccess, verifyLoginFailure, FB_APP_ID);
    setUpAnalytics(GOOGLE_ANALYTICS_ID);

    function verifyLoginSuccess() {
      $state.go('home');
    }

    function verifyLoginFailure() {
      if(!$state.current.public) {
        $state.go('login');
      }
    }
  }

  function setUpAnalytics(GOOGLE_ANALYTICS_ID) {
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments);},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m);
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

    ga('create', GOOGLE_ANALYTICS_ID, 'auto');
    ga('send', 'pageview');
  }

  function setUpFB($window, authService, verifyLoginSuccess, verifyLoginFailure, FB_APP_ID) {
    $window.fbAsyncInit = function() {
      FB.init({
        appId: FB_APP_ID,
        status: true,
        cookie: true,
        xfbml: true,
        version: 'v2.8'
      });

      authService.verifyLoggedIn(verifyLoginSuccess, verifyLoginFailure);
    };

    (function(d, s, id){
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) {return;}
      js = d.createElement(s); js.id = id;
      js.src = '//connect.facebook.net/en_US/sdk.js';
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
  }
})();

