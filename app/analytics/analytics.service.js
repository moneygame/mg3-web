(function() {
  'use strict';

  angular
    .module('moneygame.analytics')
    .service('analyticsService', analyticsService);

  function analyticsService(ENV) {
    this.setViewAnalytics = setViewAnalytics;
    this.setCountTries = setCountTries;

    function setViewAnalytics(view) {
      if (ENV == 'production') {
        ga('set', 'page', view);
        ga('send', 'pageview');
      }
    }

    function setCountTries(category, action, label, value) {
      if (ENV == 'production') {
        ga('send', 'event', category, action, label, value);
      }
    }
  }
  analyticsService.$inject = ['ENV'];
})();

