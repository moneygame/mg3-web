(function() {
  'use strict';

  angular
    .module('moneygame.profile')
    .controller('ProfileController', ProfileController);

  function ProfileController(profileService, $state, $cookies, authService, analyticsService) {
    var vm = this;
    vm.loggedIn = false;
    vm.toggleLogin = toggleLogin;
    vm.user;
    vm.getModuleLevel = getModuleLevel;

    activate();

    function activate() {
      profileService.getUser()
        .then(function (user) {
          vm.user = user;
          vm.completedSubModules = vm.user.player.completed_sub_modules || [];
          vm.loggedIn = !!vm.user.player.facebook_id;
        });

      profileService.getModuleList()
        .then(function(modules) {
          vm.modules = modules;
        });

      analyticsService.setViewAnalytics($state.current.name);
    }

    function toggleLogin() {
      vm.loggedIn ? loginFB() : logoutFB();
    }

    function logoutFB() {
      profileService.logout(logoutFBSuccess);
      var cookies = $cookies.getAll();
      angular.forEach(cookies, function (v, k) {
        $cookies.remove(k);
      });
    }

    function logoutFBSuccess(response) {
      $state.go('login');
      return response;
    }

    function loginFB() {
      authService.loginFB(loginFBSuccess, loginFBFailure);
    }

    function loginFBSuccess() {
      $state.go('home');
    }

    function loginFBFailure() {
      alert('Error logging in. Please try again.');
    }

    function getModuleLevel(moduleId) {
      var moduleLevel = filterModuleLevel(moduleId);
      return moduleLevel.level;
    }

    function filterModuleLevel(moduleId) {
      return vm.user.player.module_levels.filter(function(moduleLevel) {
        return (moduleLevel.game_module_id == moduleId);
      })[0];
    }
  }
  ProfileController.$inject = ['profileService', '$state', '$cookies', 'authService', 'analyticsService'];
})();

