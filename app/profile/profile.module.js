(function() {
  'use strict';

  angular
    .module('moneygame.profile', ['uiSwitch', 'ngCookies'])
    .config(routesConfiguration);

  function routesConfiguration($stateProvider) {
    $stateProvider
      .state('profile', {
        url: '/profile',
        cache: false,
        templateUrl: 'app/profile/profile.html',
        controller: 'ProfileController as vm'
      });
  }
  routesConfiguration.$inject = ['$stateProvider'];
})();

