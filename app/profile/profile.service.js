(function() {
  'use strict';

  angular
    .module('moneygame.profile')
    .service('profileService', profileService);

  function profileService(userService, modulesMenuService) {
    this.getModuleList = getModuleList;
    this.getUser = getUser;
    this.logout = logout;

    function getModuleList() {
      return modulesMenuService.getModulesList();
    }

    function getUser() {
      return userService.getUser();
    }

    function logout(successCallback) {
      FB.logout(function(response) {
        successCallback(response);
        userService.logout();
      });
    }
  }
  profileService.$inject = ['userService', 'modulesMenuService'];
})();


