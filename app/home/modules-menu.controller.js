(function() {
  'use strict';

  angular
    .module('moneygame.home')
    .controller('ModulesMenuController', ModulesMenuController);

  function ModulesMenuController($uibModal, $scope, $q, $state, modulesMenuService,
                                 modalService, analyticsService, gameService) {
    var vm = this;
    vm.showModulesMenu = showModulesMenu;
    vm.goToGameModule = goToGameModule;
    vm.displaySubModules = displaySubModules;
    vm.hideSubmodulesModal = hideSubmodulesModal;

    activate();

    function activate() {
      analyticsService.setViewAnalytics($state.current.name);
      modulesMenuService.getModulesList()
        .then(function(modules) {
          vm.modules = modules;
        });
    }

    function showModulesMenu() {
      vm.modulesModal = modalService.showModal($scope, true, 'app/home/modules-menu.html', 'lg');
    }

    function goToGameModule(module, index) {
      var currentModule = {
        moduleName: module.name,
        module: module.id,
        moduleVideo: module.opening_video,
        moduleClosingVideo: module.closing_video
      };
      vm.subModulesModal.close();
      gameService.setCurrentModule(currentModule, index);
      gameService.setAvailableSubmodules(vm.subModules);
      $state.go('game');
    }

    function displaySubModules(module) {
      modulesMenuService.getSubModules(module.id)
        .then(function(subModules) {
          vm.subModules = subModules;
          vm.modulesModal.close();
          vm.subModulesModal = modalService.showModal($scope, true, 'app/home/submodules-menu.html', 'lg');
        });
    }

    function hideSubmodulesModal() {
      vm.subModulesModal.close();
      showModulesMenu();
    }
  }

  ModulesMenuController.$inject = ['$uibModal', '$scope', '$q', '$state',
      'modulesMenuService', 'modalService', 'analyticsService', 'gameService'];
})();

