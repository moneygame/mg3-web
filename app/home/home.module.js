(function() {
  'use strict';

  angular
    .module('moneygame.home', [])
    .config(routesConfiguration);

  function routesConfiguration($stateProvider) {
    $stateProvider
      .state('home', {
        url: '/home',
        templateUrl: 'app/home/home.html',
        controller: 'ModulesMenuController as vm'
      });
  }
  routesConfiguration.$inject = ['$stateProvider'];
})();

