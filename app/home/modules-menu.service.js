(function() {
  'use strict';

  angular
    .module('moneygame.home')
    .service('modulesMenuService', modulesMenuService);

  function modulesMenuService($http, API_URL) {
    this.getModulesList = getModulesList;
    this.getSubModules = getSubModules;

    function getModulesList() {
      return $http.get(API_URL + 'game_modules.json')
        .then(function(response) {
          return response.data.game_modules;
        });
    }

    function getSubModules(moduleId) {
      return $http.get(API_URL + 'game_modules/' + moduleId + '.json')
        .then(function(response) {
          return response.data.game_module.sub_modules;
        });
    }
  }
  modulesMenuService.$inject = ['$http', 'API_URL'];
})();

