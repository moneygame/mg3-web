(function() {
  'use strict';

  angular
    .module('moneygame.game')
    .service('gameService', gameService);

  function gameService($q, $http, userService, API_URL) {
    this.cleanUp = cleanUp;
    this.getGameData = getGameData;
    this.getUserData = getUserData;
    this.setProgress = setProgress;
    this.updateUserGameProgress = updateUserGameProgress;
    this.markSubmoduleCompleted = markSubmoduleCompleted;
    this.setCurrentModule = setCurrentModule;
    this.getCurrentModule = getCurrentModule;
    this.setAvailableSubmodules = setAvailableSubmodules;
    this.getAvailableSubmodules = getAvailableSubmodules;
    this.hasNextSubmoduleAvailable = hasNextSubmoduleAvailable;
    this.setNextSubmodule = setNextSubmodule;
    this.setVideoClipShown = setVideoClipShown;
    this.wasVideoClipShown = wasVideoClipShown;

    var progress, isInProgress, videoClipShown = false;
    var currentModule = null;
    var currentSubmoduleIndex = null;
    var submoduleIndex = null;
    var availableSubmodules = [];

    function setAvailableSubmodules(submodules) {
      availableSubmodules = submodules;
    }

    function getAvailableSubmodules() {
      return availableSubmodules;
    }

    /**
     * Check whether a user can go to a next submodule within the same module
    */
    function hasNextSubmoduleAvailable() {
      return currentSubmoduleIndex < availableSubmodules.length - 1;
    }

    function setNextSubmodule() {
      if (hasNextSubmoduleAvailable()) {
        var nextSubmoduleIndex = currentSubmoduleIndex + 1;
        var nextModule = availableSubmodules[nextSubmoduleIndex];
        var currentModule = {
          moduleName: nextModule.name,
          module: nextModule.id,
          moduleVideo: nextModule.opening_video,
          moduleClosingVideo: nextModule.closing_video
        };
        setCurrentModule(currentModule, nextSubmoduleIndex);
      }
    }

    function setCurrentModule(module, index) {
      currentModule = module;
      currentSubmoduleIndex = index;
    }

    function getCurrentModule() {
      return currentModule;
    }

    function cleanUp() {
      videoClipShown = false;
      isInProgress = false;
      progress = null;
    }

    function setVideoClipShown() {
      videoClipShown = true;
    }

    function wasVideoClipShown() {
      return videoClipShown;
    }

    function getGameData() {
      if (isInProgress) {
        var deferred = $q.defer();
        deferred.resolve(progress);
        return deferred.promise;

      } else {
        return getQuestions()
          .then(function(_questionData_) {
            return {
              questionData: _questionData_,
              questionCounter: 0,
              points: 0
            };
          });
      }
    }

    function getUserData() {
      return userService.getUser()
        .then(function(userObj) {
          return userObj.player;
        });
    }

    function setProgress(_progress_) {
      isInProgress = true;
      progress = _progress_;
    }

    function updateUserGameProgress(data) {
      userService.updateUserGameProgress(data);
    }

    function getQuestions() {
      var moduleId = currentModule.module;
      return $http.get(API_URL + 'sub_modules/' + moduleId + '/questions.json')
        .then(function(response) {
          return response.data.questions;
        });
    }

    function markSubmoduleCompleted(playerId) {
      var moduleId = currentModule.module;
      return $http.put(API_URL + 'sub_modules/' + playerId + '/' + moduleId + '/complete');
    }
  }
  gameService.$inject = ['$q', '$http', 'userService', 'API_URL'];
})();

