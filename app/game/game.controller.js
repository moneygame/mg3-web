(function() {
  'use strict';

  angular
    .module('moneygame.game')
    .controller('GameController', GameController);

  function GameController($q, gameService, userService, modalService,
                          $state, $timeout, $log, $scope, soundService, analyticsService) {
    var vm = this;
    vm.numAnswered = 0;
    vm.questionCounter = 0;
    vm.points = 0;

    vm.goBack = goBack;
    vm.goToNextQuestion = goToNextQuestion;
    vm.goToPrevQuestion = goToPrevQuestion;
    vm.goToMainMenu = goToMainMenu;
    vm.checkAnswer = checkAnswer;
    vm.showHint = showHint;
    vm.toArray = toArray;
    vm.hideOpeningVideo = hideOpeningVideo;
    vm.goToNextSubmodule = goToNextSubmodule;
    vm.closingVideoAvailable = closingVideoAvailable;
    vm.hasNextSubmoduleAvailable = hasNextSubmoduleAvailable;
    vm.videoPlayerOpts = {
      autoplay: 1,
      controls: 0,
      rel: 0
    }

    $scope.exit = exit;

    var questionData;
    var POINTS_PER_CORRECT = 10;
    var correctToast = document.getElementById('correct-toast');
    var incorrectToast = document.getElementById('incorrect-toast');
    var countWrongAnswers = 0;

    activate();

    function activate() {
      hideToasts();
      if (!gameService.getCurrentModule()) { $state.go('home'); }
      analyticsService.setViewAnalytics($state.current.name);
      soundService.preloadSounds();
      loadGameData()
        .then(loadGameDataSuccess)
        .catch(function() {
          modalService.showModal($scope, true, 'app/game/module-completed.html', 'lg');
        });
    }

    function loadGameDataSuccess() {
      goToNextAction();
    }

    function shouldSeeOpeningVideo() {
      return vm.currentModule.moduleVideo && vm.currentModule.moduleVideo.url && vm.questionCounter === 0;
    }

    function closingVideoAvailable() {
      return vm.currentModule.moduleClosingVideo && vm.currentModule.moduleClosingVideo.url;
    }

    function showOpeningVideo() {
      vm.videoClipModal = modalService.showModal($scope, true, 'app/game/video-clip.html', 'lg');
    }

    function hideOpeningVideo() {
      vm.videoClipModal.close();
    }

    function goBack($event) {
      showExitWarning($event);
    }

    function goToNextQuestion() {
      if (vm.questionCounter < vm.numAnswered) {
        vm.questionCounter += 1;
        setNextQuestion();
      }
    }

    function goToPrevQuestion() {
      if (vm.questionCounter > 0) {
        vm.questionCounter -= 1;
        setNextQuestion();
      }
    }

    function checkAnswer(item) {

      item.answered = true;
      if (item.correct) {

        soundService.playPositiveSound();
        showToast(correctToast, 500);

        analyticsService.setCountTries('Wrong answer',
                                       gameService.getCurrentModule().moduleName,
                                       questionData[vm.questionCounter].body,
                                       countWrongAnswers);
        countWrongAnswers = 0;
        vm.questionCounter += 1;

        if (vm.questionCounter > vm.numAnswered) {
          vm.numAnswered += 1;
          vm.points += POINTS_PER_CORRECT;
        }

        saveProgress();
        $timeout(function() {
          goToNextAction();
        }, 500);

      } else {

        countWrongAnswers += 1;
        showToast(incorrectToast, 1000);
        soundService.playNegativeSound();
        saveProgress();

      }
    }

    function showHint() {
      $scope.hint = questionData[vm.questionCounter].hint;
      vm.modal = modalService.showModal($scope, true, 'app/game/hint.html', 'lg');
    }

    function goToNextAction() {
      if (vm.questionCounter < vm.TOTAL_QUESTIONS) {
        if (shouldSeeOpeningVideo()) {
          showOpeningVideo();
        }
        setNextQuestion();
      } else {
        if (!userService.getLocalPlayer().player.id) {
          handleLevelUpSuccess();
        } else {
          gameService.getUserData()
            .then(function (user) {
              gameService.markSubmoduleCompleted(user.id)
                .then(handleLevelUpSuccess)
                .catch(handleLevelUpError);
            });
        }
      }
    }

    function handleLevelUpSuccess(response) {

      gameService.cleanUp();
      gameService.updateUserGameProgress({
        levelIncrease: 1,
        pointsIncrease: vm.points
      });
      vm.levelUpModal = modalService.showModal($scope, true, 'app/game/level-complete.html', 'lg');
    }

    function handleLevelUpError(response) {
      $log.error(response.data);
    }

    function loadGameData() {
      vm.currentModule = gameService.getCurrentModule();
      var deferred = $q.defer();

      gameService.getGameData()
        .then(function(gameData) {
          if(gameData.questionData) {
            vm.questionCounter = gameData.questionCounter;
            vm.numAnswered = vm.questionCounter;
            questionData = gameData.questionData;
            vm.TOTAL_QUESTIONS =  questionData.length;
            vm.points = gameData.points;
            deferred.resolve();
          } else {
            deferred.reject();
          }
        });
      return deferred.promise;
    }

    function saveProgress() {
      gameService.setProgress({
        questionCounter: vm.questionCounter,
        questionData: questionData,
        points: vm.points
      });
    }

    function setNextQuestion() {
      vm.question = questionData[vm.questionCounter].body;
      vm.answerOptions = questionData[vm.questionCounter].options;
      vm.hint = questionData[vm.questionCounter].hint;
    }

    function showToast(toast, duration) {
      hideToasts();

      toast.removeAttribute('style');
      $timeout(function() {
        hideToasts();
      }, duration);
    }

    function hideToasts() {
      correctToast.style.top = '-9999px';
      incorrectToast.style.top = '-9999px';
    }

    function toArray(number) {
      var array = [];
      for (var i=0; i<number; i++) {
        array.push(i);
      }
      return array;
    }

    function exit() {
      gameService.cleanUp();
      vm.modal.close();
      $state.go('home');
    }

    function showExitWarning() {
      vm.modal = modalService.showModal($scope, true, 'app/game/exit-alert.html', 'lg');
    }

    function goToMainMenu() {
      $state.go('home');
    }

    function goToNextSubmodule() {
      gameService.cleanUp();
      gameService.setNextSubmodule();
      vm.numAnswered = 0;
      vm.questionCounter = 0;
      vm.points = 0;
      vm.levelUpModal.close();
      activate();
    }

    function hasNextSubmoduleAvailable() {
      return gameService.hasNextSubmoduleAvailable();
    }
  }
  GameController.$inject = ['$q', 'gameService', 'userService',
      'modalService', '$state', '$timeout', '$log', '$scope', 'soundService',
      'analyticsService'];
})();

