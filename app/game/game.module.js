(function() {
  'use strict';

  angular
    .module('moneygame.game', ['youtube-embed'])
    .config(routesConfiguration);

  function routesConfiguration($stateProvider) {
    $stateProvider
      .state('game', {
        cache: false,
        url: '/game',
        templateUrl: 'app/game/game.html',
        controller: 'GameController as vm'
      })
      .state('levelComplete', {
        url: '/',
        templateUrl: 'app/game/level-complete.html'
      });
  }
  routesConfiguration.$inject = ['$stateProvider'];
})();

