# MoneyGame3 Web

Requirements: npm

## Installation
1. `$ git clone https://bitbucket.org/moneygame/mg3-web.git`
2. `$ cd mg3-web`
3. `$ npm install`
4. edit .env.template and rename to .env

## Scripts
- `$ npm start`: build dist and run production server
- `$ npm run dev`: run development server (watches sass files)
- `$ npm run build-css`: builds style.css from sass files
- `$ npm run watch-css`: watches for changes in sass files and compiles to style.css
- `$ npm run build-dist`: build css, config, uglify js and move files to www/

